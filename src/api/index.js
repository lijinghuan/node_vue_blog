import ajax from './ajax'

const BASE_URL = 'https://vueblog.tiandaodao.com';

export const pwdLogin = (params)=> ajax(BASE_URL + '/login', params, 'POST');//登录

export const getAllUserList = ()=> ajax(BASE_URL + '/getAllUserList')//获取所有用户 

export const addUserInfo = (params)=> ajax(BASE_URL + '/addUser', params, 'POST')//新建用户

export const deleteUserInfo = (params)=> ajax(BASE_URL + '/deleteUserInfo', params, 'POST')//删除用户信息

export const modifyUserInfo = (params)=> ajax(BASE_URL + '/modifyUserInfo', params, 'POST')//修改用户
