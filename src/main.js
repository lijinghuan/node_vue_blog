import Vue from 'vue'
import App from './App.vue'
import store from './store'
import router from './router'
import './assets/css/common.css'
import './assets/fonts/iconfont.css'

// axios注入vue 原型中
import axios from 'axios'
Vue.prototype.$http = axios

// 全局 element-ui
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
Vue.use(ElementUI);
Vue.config.productionTip = false

// 请求拦截器
axios.interceptors.request.use(function (config) {
    // header 增 token验证

    let token = localStorage.getItem('token')
    if (token && config.url!="https://vueblog.tiandaodao.com/login") {
      config.headers.token = token
    }
// <<<<<<< HEAD
// =======
//     console.log(config.url)
// >>>>>>> a4f28917ff8329372639eecd4a30e0a347311cf7
    // 在发送请求之前做些什么
    return config;
  }, function (error) {
    console.log(error)
    return Promise.reject(error);
  });

// 响应拦截器
axios.interceptors.response.use(function (response) {
     if (response.status != 200) {
        ElementUI.Message({
            message:response.data.message,
            type:'error'
          })
     }else{
        if (response.data.code!=200) {
          ElementUI.Message({
            message:response.data.message,
            type:'error'
          })
        }
     }
    return response;
  }, function (error) {
    console.log(error)
    return Promise.reject(error);
  });

new Vue({
    router,
    store,
    render: h => h(App)
}).$mount('#app')