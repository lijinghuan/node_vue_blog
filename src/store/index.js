import Vue from 'vue'
import Vuex from 'vuex'
Vue.use(Vuex)

import { getAllUserList } from '../api/index'
import {
    USER_INFO, ALL_USER_LIST
} from './mutations-types';

export default new Vuex.Store({
    state: {
        userinfo: {}, alluserlist: []
    },
    mutations: {
        [USER_INFO](state, {userinfo}) {
            state.userInfo = userinfo;
        },
        [ALL_USER_LIST](state,{alluserlist}) {
            state.alluserlist = alluserlist;
        }
    },
    actions: {
        syncUserInfo({commit}, userInfo) {
            commit(USER_INFO, {userInfo});
        },
        async reqAllUserList({commit}){
            const result = await getAllUserList();
            commit(ALL_USER_LIST,{alluserlist:result.data});
        }
    },
})