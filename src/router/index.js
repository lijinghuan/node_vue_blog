import Vue from 'vue'
import VueRouter from 'vue-router'
import Login from '@/components/common/login'
import Index from '@/components/index'
import User from '@/components/user/user'
import Tag from '@/components/tag/tag'
Vue.use(VueRouter)

const routes = [
    {
        path: '/',
        redirect: '/login',
        component: Login
    },
    {
        path: '/login',
        name: 'Login',
        component: Login
    },
    {
        path: '/index',
        component:Index,
        children:[
            {
                path:'',
                redirect:'/index/user'
            },{
                path: 'user',
                name: 'User',
                component:User
            },{
                path: 'tag',
                name: 'Tag',
                component:Tag
            }, 
        ]
    },
    
]

const router = new VueRouter({
    routes
})


//路由导航守卫
router.beforeEach((to, from, next) => {
    if (to.path === '/login') return next()
    const tokenStr = window.localStorage.getItem('token')
    if (!tokenStr) return next('/login')
    next()
})
export default router